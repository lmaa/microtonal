import QtQuick 2.0
import QtQuick.Controls 1.0
import MuseScore 1.0

MuseScore {
  version:  "1.0"
  description: "Microtonal tunings in playback"
  menuPath: "Plugins.Microtonal"

  pluginType: "dock"
  dockArea:   "right"

  /* anchors.centerIn: parent */

  onRun: {
    console.log("Microtonal fixups");


  }
  width: 230
  height: 1200
  anchors.fill: parent

  Column {
    id: column1
    anchors.fill: parent
    spacing: 5

    Row {
      id: row1
      height: 30
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.right: parent.right
      anchors.rightMargin: 0

      Text {
        text: "mirrored flat2"
      }

      SpinBox {
        id: mirroredflat2
        height: 25
        anchors.verticalCenter: parent.verticalCenter
        width: 75
        stepSize: 1.0
        maximumValue: 1000.0
        minimumValue: -1000.0
        value: -150.0
        anchors.right: parent.right
        anchors.rightMargin: 0
      }
    }

    Row {
      id: row23
      height: 30
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0

      Text {
        text: "flat arrow down"
      }
      SpinBox {
        id: flatarrowdown
        height: 25
        anchors.verticalCenter: parent.verticalCenter
        width: 75
        stepSize: 1.0
        value: -125.0
        maximumValue: 1000.0
        minimumValue: -1000.0
        anchors.right: parent.right
        anchors.rightMargin: 0


      }
    }

    Row {
      id: row2
      height: 30
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0

      Text {
        text: "flat arrow up"
      }
      SpinBox {
        id: flatarrowup
        height: 25
        anchors.verticalCenter: parent.verticalCenter
        width: 75
        stepSize: 1.0
        value: -75.0
        maximumValue: 1000.0
        minimumValue: -1000.0
        anchors.right: parent.right
        anchors.rightMargin: 0
        horizontalAlignment: Text.AlignRight


      }
    }

    Row {
      id: row3
      height: 30
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0
      Text {
        text: "mirrored flat"
      }
      SpinBox {
        id: mirroredflat
        height: 25
        anchors.verticalCenter: parent.verticalCenter
        width: 75
        stepSize: 1.0
        value: -50.0
        maximumValue: 1000.0
        minimumValue: -1000.0
        anchors.right: parent.right
        anchors.rightMargin: 0
        horizontalAlignment: Text.AlignRight
      }
    }
    Row {
      id: row4
      height: 30
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0


      Text {
        text: "natural arrow down"
      }
      SpinBox {
        id: naturalarrowdown
        height: 25
        anchors.verticalCenter: parent.verticalCenter
        stepSize: 1.0
        width: 75
        value: -25.0
        maximumValue: 1000.0
        minimumValue: -1000.0
        anchors.right: parent.right
        anchors.rightMargin: 0
        horizontalAlignment: Text.AlignRight
      }
    }
    Row {
      id: row6
      height: 30
      anchors.left: parent.left
      anchors.leftMargin: 0
      Text {
        text: "natural arrow up"
      }

      SpinBox {
        id: naturalarrowup
        height: 25
        anchors.verticalCenter: parent.verticalCenter
        width: 75
        stepSize: 1.0
        value: 25
        maximumValue: 1000
        horizontalAlignment: Text.AlignRight
        minimumValue: -1000
        anchors.right: parent.right
        anchors.rightMargin: 0
      }
      anchors.right: parent.right
      anchors.rightMargin: 0
    }


    Row {
      id: row7
      height: 30
      anchors.left: parent.left
      anchors.leftMargin: 0
      Text {
        text: "sharp slash"
      }

      SpinBox {
        id: sharpslash
        width: 75
        height: 25
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 0
        stepSize: 1.0
        value: 50
        maximumValue: 1000
        minimumValue: -1000
      }
      anchors.right: parent.right
      anchors.rightMargin: 0
    }

    Row {
      id: row8
      height: 30
      anchors.left: parent.left
      anchors.leftMargin: 0
      Text {
        text: "sharp arrow down"
      }

      SpinBox {
        id: sharparrowdown
        height: 25
        anchors.verticalCenter: parent.verticalCenter
        width: 75
        stepSize: 1.0
        value: 75
        maximumValue: 1000
        horizontalAlignment: Text.AlignRight
        minimumValue: -1000
        anchors.right: parent.right
        anchors.rightMargin: 0
      }
      anchors.right: parent.right
      anchors.rightMargin: 0
    }

    Row {
      id: row9
      height: 30
      anchors.left: parent.left
      anchors.leftMargin: 0
      Text {
        text: "sharp arrow up"
      }

      SpinBox {
        id: sharparrowup
        height: 25
        anchors.verticalCenter: parent.verticalCenter
        width: 75
        stepSize: 1.0
        value: 125
        maximumValue: 1000
        horizontalAlignment: Text.AlignRight
        minimumValue: -1000
        anchors.right: parent.right
        anchors.rightMargin: 0
      }
      anchors.right: parent.right
      anchors.rightMargin: 0
    }

    Row {
      id: row10
      height: 30
      anchors.left: parent.left
      anchors.leftMargin: 0
      Text {
        text: "mirrored sharp4"
      }

      SpinBox {
        id: mirroredsharp4
        height: 25
        anchors.verticalCenter: parent.verticalCenter
        width: 75
        stepSize: 1.0
        value: 150
        maximumValue: 1000
        horizontalAlignment: Text.AlignRight
        minimumValue: -1000
        anchors.right: parent.right
        anchors.rightMargin: 0
      }
      anchors.right: parent.right
      anchors.rightMargin: 0
    }


    Row {
      id: row5
      height: 30
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.right: parent.right
      anchors.rightMargin: 0
      Action {
        id: tuneAction
        text: "Tune"
        onTriggered: {
          if (typeof curScore === 'undefined')
            Qt.quit();

          for (var track = 0; track < curScore.ntracks; ++track) {
            var segment = curScore.firstSegment();
            while (segment) {
              var element = segment.elementAt(track);
              if (element ) {
                var type    = element.type;
                if (type == Element.CHORD) {

                  for (var j = 0; j < element.notes.length; j++) {
                    var note = element.notes[j];
                    if (note.accidental) {
                      var ac = note.accidental;
                      var at = note.accidentalType;
                      var tuning = 0.0;
                      switch (at) {
                      case Accidental.SHARP_ARROW_UP:
                        tuning = sharparrowup.value;
                        break;
                      case Accidental.SHARP_SLASH:
                        tuning = sharpslash.value;
                        break;
                        /* case Accidental.FLAT_SLASH: */
                        /*   tuning = flatslash.value; */
                        /*   break; */
                      case Accidental.MIRRORED_FLAT:
                        tuning = mirroredflat.value;
                        break;
                      case Accidental.MIRRORED_FLAT2:
                        tuning = mirroredflat2.value;
                        break;
                      case Accidental.SHARP_SLASH4:
                        tuning = mirroredsharp4.value;
                        break;
                      case Accidental.FLAT_ARROW_UP:
                        tuning = flatarrowup.value;
                        break;
                      case Accidental.FLAT_ARROW_DOWN:
                        tuning = flatarrowdown.value;
                        break;
                      case Accidental.NATURAL_ARROW_UP:
                        tuning = naturalarrowup.value;
                        break;
                      case Accidental.NATURAL_ARROW_DOWN:
                        tuning = naturalarrowdown.value;
                        break;
                      case Accidental.SHARP_ARROW_UP:
                        tuning = sharparrowup.value;
                        break;
                      case Accidental.SHARP_ARROW_DOWN:
                        tuning = sharparrowdown.value;
                        break;

                      }
                      console.log(tuning, at, ac.symbol);
                      note.tuning = tuning;
                    }
                  }
                }
              }
              segment = segment.next;
            }
          }

        }

      }
      Button {
        text: "Tune"
        action: tuneAction
      }
    }
  }
}
